from setuptools import setup

setup(
    name='SimpleConfig',
    version='0.1',
    py_modules=['simpleconfig'],
    include_package_data=True,
)
