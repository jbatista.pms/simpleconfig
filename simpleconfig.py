import json, os
from collections import OrderedDict
from typing import Any, Tuple


class Field(object):
    value = None

    def __init__(self, default=None, null=False, required=False) -> None:
        self.null = null
        self.required = required
        self.value = default
    
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(value={self.value})"
    
    def __str__(self) -> str:
        return str(self.value)
    
    def get_value(self) -> Any:
        return self.value
    
    def set_value(self, value: Any) -> None:
        self.value = self.validate(value)

    def validate(self, value: Any) -> Any:
        if value is None:
            if self.null:
                return value
            raise ValueError(f"Value cannot be null.")
        return value


class BooleanField(Field):
    def get_value(self) -> int:
        return self.value
    
    def validate(self, value: Any) -> Any:
        value = super().validate(value=value)
        if isinstance(value, bool):
            return value
        raise ValueError(f"Value '{value}' not is boolean.")


class IntegerField(Field):
    def get_value(self) -> int:
        return self.value
    
    def validate(self, value: Any) -> Any:
        value = super().validate(value=value)
        if isinstance(value, int):
            return value
        raise ValueError(f"Value '{value}' not is integer.")


class StringField(Field):
    def get_value(self) -> str:
        return self.value
    
    def validate(self, value: Any) -> Any:
        value = super().validate(value=value)
        if isinstance(value, str):
            return value
        raise ValueError(f"Value '{value}' not is string.")


class Node(object):
    __initialized = False

    def __init__(self) -> None:
        self.__extended = OrderedDict()
        self.__fields = OrderedDict()
        for k,v in self.__class__.__dict__.items():
            if isinstance(v, tuple(Field.__subclasses__())):
                self.__fields[k] = v
                setattr(self, k, v.get_value())
            elif isinstance(v, ExtendedField):
                self.__extended[k] = v
                setattr(self, k, v.get_value())
        self.__initialized = True
    
    def __setattr__(self, name: str, value: Any) -> None:
        if self.__initialized:
            if name in self.__fields:
                self.__fields[name].set_value(value)
            elif name in self.__extended:
                raise Exception("ExtendedField cannot be changed!")
        super().__setattr__(name, value)
    
    def dicts(self) -> dict:
        cfg_dict = OrderedDict()
        for k,v in self.__extended.items():
            cfg_dict.update({k:v.get_value().dicts()})
        for k,v in self.__fields.items():
            cfg_dict.update({k:v.get_value()})
        return cfg_dict
    
    def load(self, cfg):
        for k,v in cfg.items():
            if k in self.__fields:
                self.__fields[k].set_value(v)
                setattr(self, k, v)
            elif k in self.__extended:
                self.__extended[k].node.load(v)
    
    def validate(self) -> Tuple[bool, list]:
        errors_dict = OrderedDict()
        for k,v in self.__extended.items():
            errors = v.node.validate()
            if errors:
                errors_dict[k] = errors
        for k,v in self.__fields.items():
            try:
                v.validate(v.get_value())   
            except Exception as error:
                errors_dict[k] = str(error)
        return errors_dict or None


class ExtendedField(object):
    def __init__(self, node: Node) -> None:
        self.node = node()
    
    def get_value(self) -> Node:
        return self.node


class Configuration(object):
    errors = None

    def __init__(self, node: Node) -> None:
        self.root = node()
    
    def load(self, file: str) -> None:
        if os.path.isfile(file):
            with open(file, 'r') as arq:
                self.root.load(json.load(arq))
    
    def save(self, file) -> None:
        self.errors = self.root.validate()
        if self.errors:
            raise Exception("Errors found.")
        with open(file, 'w') as cfg:
            json.dump(self.root.dicts(), cfg, indent=4)