import unittest
from simpleconfig import *


class PostgreSQLNode(Node):
    access = BooleanField(default=False, required=True)
    host = StringField(required=True)
    port = IntegerField(required=True)


class RootNode(Node):
    postgresql = ExtendedField(PostgreSQLNode)


class CfgTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.cfg = Configuration(RootNode)
        self.cfg_file = os.path.join(os.getcwd(), 'cfg.json')
    
    def test_set(self):
        self.assertEqual(self.cfg.root.postgresql.host, None)
        self.assertEqual(self.cfg.root.postgresql.access, False)
        self.assertEqual(self.cfg.root.postgresql.port, None)
        self.cfg.load(self.cfg_file)
        self.cfg.root.postgresql.host = 'localhost'
        self.cfg.root.postgresql.access = True
        self.cfg.root.postgresql.port = 5
        self.assertEqual(self.cfg.root.postgresql.host, 'localhost')
        self.assertEqual(self.cfg.root.postgresql.access, True)
        self.assertEqual(self.cfg.root.postgresql.port, 5)
        self.cfg.save(self.cfg_file)